# Tpflow 3.0 正式版

**欢迎使用 Tpflow 工作流引擎**

![star](https://gitee.com/ntdgg/tpflow/badge/star.svg?theme=dark "tpflow") ![fork](https://gitee.com/ntdgg/tpflow/badge/fork.svg "tpflow") 

### 有问题请提交ISSUE 48小时内解决~~

### Laravel版本的工作流

[Laravel版本](http://gadmin.cojz8.com/group-topic-id-25.html "Laravel版本")

### 主要特性

+ 基于  `<jsPlumb>` 可视化设计流程图
    + 支持可视化界面设计
    + 支持拖拽式流程绘制
    + 三布局便捷调整
    + 基于`workflow.3.0.js` `workflow.3.0.js ` 引擎
+ 超级强大的API 对接功能
    + `flowApi` 可支持工作流设计开发管理
    + `ProcessApi` 步骤管理API，可以对步骤进行管理、读取
    + `SuperApi ` 超级管理接口，对流程进行终止，代审
+ 完善的流引擎机制
    + 规范的命名空间，可拓展的集成化开发
    + 支持 直线式、会签式、转出式、同步审批式等多格式的工作流格式
+ 提供基于 `Thinkphp5.1.X` 的样例Demo
+ 提供完整的设计手册

### 在线文档

[看云文档](https://www.kancloud.cn/guowenbin/tpflow "安装手册")   [官方博客](http://www.cojz8.com/tag/30 "官方博客")

### 在线演示

[http://tpflow.cojz8.com](http://tpflow.cojz8.com "http://tpflow.cojz8.com")   

提　示：演示站供各位测试使用，请勿随意填写数据操作。

### 界面截图

![markdown](http://files.git.oschina.net/group1/M00/06/3A/PaAvDFw4NRKAK6CCAAEZKRKE9TE045.png?token=cc97060f3fa5ed3cb7356ccdab6b10ae&ts=1547187474&attname=1.png "tpflow")

### 问题反馈
我们建议直接提交 ISSUE 问题反馈给我们，或者加入我们QQ群

>在使用中有任何问题，请使用以下联系方式联系我们


### 相关链接
---

> 官方博客：http://www.cojz8.com/

> 官方博客：http://tpflow.cojz8.com/   

> 工作流手册：https://www.kancloud.cn/guowenbin/tpflow

> 视频教程：http://www.cojz8.com/article/86

---

## 版权信息

Tpflow 遵循 MIT 开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2018-2020 by Tpflow (http://cojz8.com)

All rights reserved。

~~~
对您有帮助的话，你可以在下方赞助我们，让我们更好的维护开发，谢谢！
~~~

~~~
对您有帮助的话，你可以在下方赞助我们，让我们更好的维护开发，谢谢！
~~~